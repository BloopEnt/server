-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 28, 2018 at 11:13 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `straight_up`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category_name` varchar(256) NOT NULL,
  `bg_color` varchar(16) DEFAULT NULL,
  `image` varchar(256) DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1: Active, 0 : Inactive',
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `bg_color`, `image`, `status`, `created_date`, `modified_date`) VALUES
(1, 'test', '#da1414', '1519389329820593384_Screenshot_from_2018-02-12_19-09-24.png', 1, '2018-02-22 18:30:00', NULL),
(2, 'first category', '#324c35', '1519639853265550159_Screenshot_from_2017-10-31_11-52-54.png', 1, '2018-02-26 08:35:55', '2018-02-26 08:35:55'),
(3, 'third category ', '#a85050', '15196476271251152101_Screenshot_from_2017-10-31_11-52-54.png', 0, '2018-02-26 12:20:28', '2018-02-26 12:20:28'),
(4, 'test category four', '#2c7f80', '1519652435304850861_Screenshot_from_2018-02-12_11-50-49.png', 0, '2018-02-26 13:40:35', '2018-02-26 13:40:35'),
(5, 'ewrr', '#000000', '15196525741664140437_Screenshot_from_2018-02-12_19-09-24.png', 0, '2018-02-26 13:42:54', '2018-02-26 13:42:54'),
(6, 'hello  testing', '#7f4949', '1519705576866090665_Screenshot_from_2017-11-22_11-00-24.png', 0, '2018-02-27 04:26:16', '2018-02-27 04:26:16'),
(7, 'testing 2', '#9e8181', '15197056501350362077_Screenshot_from_2017-10-16_13-32-28.png', 0, '2018-02-27 04:27:31', '2018-02-27 04:27:31'),
(8, 'qwerty', '#378ccc', '1519705823502304060_Screenshot_from_2017-10-09_10-30-29.png', 0, '2018-02-27 04:30:23', '2018-02-27 04:30:23'),
(9, 'asddf', '#9c3d3d', '15197061012122198339_Screenshot_from_2017-11-14_16-45-27.png', 0, '2018-02-27 04:35:01', '2018-02-27 04:35:01'),
(10, 'Primetime', '#c99696', '151973332892844626_Screenshot_from_2017-11-16_16-36-04.png', 0, '2018-02-27 04:35:44', '2018-02-27 04:35:44'),
(11, 'category by parul test', '#777777', '15197392141266787114_Screen_Shot_2018-02-27_at_7_15_09_PM.png', 0, '2018-02-27 13:46:54', '2018-02-27 13:46:54');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `sub_cat_id` int(11) NOT NULL,
  `status` int(11) DEFAULT '1',
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `name`, `sub_cat_id`, `status`, `created_date`, `updated_date`) VALUES
(1, 'Test question2', 1, 1, '2018-02-26 06:03:57', '2018-02-26 06:03:57'),
(2, 'Test question again 123', 1, 1, '2018-02-26 08:10:57', '2018-02-26 08:10:57'),
(3, 'first question for testing ', 6, 1, '2018-02-27 06:11:14', '2018-02-27 06:11:14'),
(4, 'add more questions for testing add more questions for testing ', 6, 1, '2018-02-27 06:49:49', '2018-02-27 06:49:49'),
(5, 'add question in dummy id', 3, 1, '2018-02-27 06:59:21', '2018-02-27 06:59:21'),
(6, 'Add question in dummy sub category and Zxccvb parent category', 4, 1, '2018-02-27 07:18:02', '2018-02-27 07:18:02'),
(7, 'name of the technologies in which they are working', 7, 1, '2018-02-27 07:26:48', '2018-02-27 07:26:48'),
(8, 'number of employess', 7, 1, '2018-02-27 07:27:09', '2018-02-27 07:27:09'),
(9, 'test question 1', 8, 1, '2018-02-27 13:48:43', '2018-02-27 13:48:43'),
(10, 'Test question 2', 8, 1, '2018-02-27 13:48:54', '2018-02-27 13:48:54'),
(11, 'Test question 3', 8, 1, '2018-02-27 13:49:05', '2018-02-27 13:49:05'),
(12, 'Test question4', 8, 1, '2018-02-27 13:49:16', '2018-02-27 13:49:16');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `price` int(11) DEFAULT '0',
  `paid` int(11) DEFAULT '0',
  `bg_color` varchar(32) NOT NULL,
  `ios_app_id` varchar(256) DEFAULT NULL,
  `andrdoid_app_id` varchar(256) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `name`, `parent_id`, `price`, `paid`, `bg_color`, `ios_app_id`, `andrdoid_app_id`, `status`, `created_date`, `updated_date`) VALUES
(1, 'sub test2', 1, 10, 0, '#6d6214', NULL, NULL, 1, '2018-02-26 05:29:32', '2018-02-26 05:29:32'),
(2, 'sub2', 2, 0, 1, '#2e2165', NULL, NULL, 1, '2018-02-26 10:12:40', '2018-02-26 10:12:40'),
(3, 'third sub category', 3, 0, 1, '#50649b', '2', '3', 1, '2018-02-26 13:20:15', '2018-02-26 13:20:15'),
(4, 'sub category three', 2, 0, 1, '#a362ac', 'dfddsf', 'gbf', 1, '2018-02-26 13:22:44', '2018-02-26 13:22:44'),
(5, 'dummy', 10, 0, 0, '#b97b7b', 'ghjjk', 'gffjhkk', 1, '2018-02-27 04:45:24', '2018-02-27 04:45:24'),
(6, 'testing more', 10, 10, 1, '#5b3535', '', '', 1, '2018-02-27 04:52:10', '2018-02-27 04:52:10'),
(7, 'skycap', 10, 20, 1, '#9a3c1e', 'sdas', 'sdasdsa', 1, '2018-02-27 07:26:03', '2018-02-27 07:26:03'),
(8, 'Test Sub cat', 11, 0, 0, '#111111', '', '', 1, '2018-02-27 13:47:50', '2018-02-27 13:47:50');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` text NOT NULL,
  `status` int(11) DEFAULT '1',
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `created_date`, `updated_date`) VALUES
(1, 'Admin', 'geeta@skycap.co.in', '649c70f7e69bfc692b2dd417bda0745a', 1, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
