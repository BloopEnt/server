Here is the structure for categories, subcategories and questions array:

Category:
[{
    "uid": "projects/charades-dev/databases/(default)/documents/categories/23iyKGai1622f4PLBBbK",
    "name": "Primetime",
    "background": "#ff572d",
    "icon": "https://firebasestorage.googleapis.com/v0/b/charades-dev.appspot.com/o/television.png?alt=media&token=b75e651b-7068-4833-9805-0254bc39f528",
    "createTime": "2018-01-19T12:25:34.993398Z",
    "updateTime": "2018-02-12T05:48:08.956528Z"
}]



SubCategory:
[{
    "uid": "projects/charades-dev/databases/(default)/documents/subcategories/3SO1Y1zFlFoCnYAiICec",
    "iosInAppId": "",
    "name": "Will Smith",
    "andrdoidInAppId": "",
    "category": "projects/charades-dev/databases/(default)/documents/categories/JP1ddSMuqsZflsZrJ56U",
    "price": "0",
    "paid": false,
    "createTime": "2018-02-16T05:28:09.055467Z",
    "updateTime": "2018-02-16T07:52:00.546255Z",
    "background": "#B8860B"
}]


Question:
[{
    "uid": "projects/charades-dev/databases/(default)/documents/questions/0TRRcD8dGTk01b7llEwd",
    "name": "Kidnap",
    "subcategory": "projects/charades-dev/databases/(default)/documents/subcategories/frhmrj1Q3cNyOcz64F0I",
    "createTime": "2018-02-16T06:37:18.888068Z",
    "updateTime": "2018-02-16T06:57:10.372886Z"
}]
