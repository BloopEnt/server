 <div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" data-background-color="purple">
						<h4 class="title">Import Questions</h4>
					</div>
					<div class="card-content">
						<?php if (isset($error)) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-danger fade in">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<?php echo $error; ?>
									</div>
								</div>
							</div>
						<?php } ?>
						<form action="import" method="post" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Category <span style="color : red;">*</span></label>
										<select name="category" class="col-md-12 form-control" id="parent-category" required>
										<?php if(count($categories)) { ?>
											<?php foreach($categories as $key => $val) { ?>
												<option value="<?php echo $val->id;?>"> <?php echo ucfirst($val->category_name);?> </option>
											<?php } ?>
										<?php } ?>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label"> Sub Category <span style="color : red;">*</span></label>
										<select name="sub_category" class="col-md-12 form-control" id="child-categories" required>
										<?php if(count($sub_categories)) { ?>
											<?php foreach($sub_categories as $key => $val) { ?>
												<option value="<?php echo $val->id;?>"> <?php echo ucfirst($val->name);?> </option>
											<?php } ?>
										<?php } ?>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Choose File <span style="color : red;">*</span> </label>
										<input type="file" class="form-control upload-image" name="excel-sheet" required>
									</div>
								</div>
							</div>
							
							<button type="submit" class="btn btn-primary pull-right">Save</button>
							<div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
          
