 <div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" data-background-color="purple">
						<h4 class="title">View Question</h4>
					</div>
					<div class="card-content">
						<?php if (isset($error)) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-danger fade in">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<?php echo $error; ?>
									</div>
								</div>
							</div>
						<?php } else { ?>
						
							<form action="" method="post" enctype="multipart/form-data">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group label-floating">
											<label class="control-label">Category Name</label>
											<span class="form-control"> <?php echo ucfirst($question->cat_name); ?></span>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group label-floating">
											<label class="control-label"> Sub Category Name</label>
											<span class="form-control"> <?php echo ucfirst($question->s_name); ?></span>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group label-floating">
											<label class="control-label">Question</label>
											<span class="form-control"><?php echo ucfirst($question->q_name); ?></span>
										</div>
									</div>
								</div>
								
								<div class="row">	
									<div class="col-md-6">
										<div class="form-group label-floating">
											<label class="control-label">Status</label>
											<span class="form-control"> <?php echo ($question->status == 1) ? 'Active' : 'Inactive';?></span>
										</div>
									</div>
								</div>
							</form>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
          
