 <div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" data-background-color="purple">
						<h4 class="title">Add More Questions</h4>
					</div>
					<div class="card-content">
						<?php if (isset($error)) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-danger fade in">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<?php echo $error; ?>
									</div>
								</div>
							</div>
						<?php } ?>
						<form action="/index.php/questions/add" method="post" enctype="multipart/form-data">
							
							<div class="row">
								<div class="col-md-6">
									<label class="control-label"> Category Name<span style="color : red;">*</span></label>
									<span  class="form-control" ><?php echo ucfirst($question->cat_name);?></span> 
								</div>
							</div>	
							<div class="row">
								<div class="col-md-6">
									<label class="control-label"> Sub Category Name<span style="color : red;">*</span></label>
									<span  class="form-control" ><?php echo ucfirst($question->s_name);?></span> 
								</div>
							</div>	
							<div class="row last-added-question">
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Question <span style="color : red;">*</span></label>
										<span  class="form-control" ><?php echo ucfirst($question->q_name);?></span> 
									</div>
								</div>
							</div>
							<div class="row new-question-add hide">
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Question <span style="color : red;">*</span></label>
										<input type="text" class="form-control required" name="name">
									</div>
								</div>
							</div>
							
							<input type="hidden" name="sub_category" value="<?php echo $question->sub_cat_id;?>">
							<input type="hidden" name="category" value="<?php echo $question->cat_id;?>"> 
							<a href="/index.php/questions/index" class="btn btn-default pull-right">cancel</a>
							<button type="button" class="btn btn-primary pull-right" id="add-more-questions">Add More Questions</button>
							<button type="save" class="btn btn-primary pull-right hide" id="save-questions">save</button>
							<a href="/index.php/questions/edit/<?php echo $question->id;?>" class="btn btn-warning pull-right">edit</a>
							<div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
          
