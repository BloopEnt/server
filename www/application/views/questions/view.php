<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header row" data-background-color="purple">
						
						<h4 class="title col-sm-4">Questions <?php if($parentCategoryName != null) { echo " - ".ucfirst($parentCategoryName); }?></h4>
						
						<div class="col-sm-8">
							<div class="col-md-5">
								<input type="text" class="form-control col-md-11" style="margin-top:-11px; color:#fff;" name="search" id="search" placeholder="Enter question here to search" value="<?php echo $search;?>"/>
							</div>
							<div class="col-md-3">
								<span class="cross hide" style="margin-right:10px;"><b>X</b></span>
								<button class="btn btn-default btn-md" > Go </button>
							</div>
							<div class="col-md-4">
								<a class="pull-right btn btn-default" href="<?php echo ($subID != null) ? 'import?id='.$subID : 'import'; ?>" >  Import </a>
								<a class="pull-right btn btn-primary" href="<?php echo ($subID != null) ? 'add?id='.$subID : 'add'; ?>" >  Add </a>
							</div>
						</div>
					</div>
					<div class="card-content table-responsive">
						<table class="table">
							<thead class="text-primary">
								<th class="col-md-4">Name</th>
								<th class="col-md-2">Category</th>
								<th class="col-md-2">Sub Category</th>
								<th class="col-md-1">Status</th>
								<th class="col-md-1">Created Date</th>
								<th class="col-md-2">Action</th>
							</thead>
							<tbody>
							<?php if(count($questions)) { ?>
								<?php foreach($questions as $question) {?>
								<tr>
									<td><?php echo $question->name;?></td>
									<td> <a href="/index.php/categories/view/<?php echo $question->category->id; ?>"><?php echo ucfirst($question->category->category_name);?></a></td>
									<td> <a href="/index.php/subCategories/view/<?php echo $question->sub_cat_id; ?>"><?php echo ucfirst($question->sub_category->name);?></a></td>
									<td><?php echo ($question->status == 1) ? 'Active' : 'Inactive';?></td>
									<td><?php echo date('m/d/Y',strtotime($question->created_date));?></td>
									<td class="text-primary"><?php 
										$questionID = $question->id;
										if($subID != null) {
											echo anchor('questions/view/' . $questionID.'?id='.$subID, '<i class="fa fa-eye"></i>', array('class' => "btn btn-success btn-sm", 'title' => 'View'));
											echo "&nbsp;";
											echo anchor('questions/edit/' . $questionID.'?id='.$subID, '<i class="fa fa-pencil"></i>', array('class' => "btn btn-primary btn-sm", 'title' => 'Edit'));
										} else { 
											echo anchor('questions/view/' . $questionID, '<i class="fa fa-eye"></i>', array('class' => "btn btn-success btn-sm", 'title' => 'View'));
											echo "&nbsp;";
											echo anchor('questions/edit/' . $questionID, '<i class="fa fa-pencil"></i>', array('class' => "btn btn-primary btn-sm", 'title' => 'Edit'));
										}
									?>
									</td>
								</tr>
							<?php
								}
							 } else { ?>
								<tr colspan="7"> No Record found in this table</tr>
							 
						<?php } ?>
							</tbody>
						</table>
						 <?php if (isset($links)) { 
									echo $links; 
								} ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
                       
