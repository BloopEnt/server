 <div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header row" data-background-color="purple">
						<div class="col-sm-6">
							<h4 class="title">Add Question</h4>
						</div>
						<div class="col-sm-6">
							<a class="pull-right btn btn-default" href="import" >  Import </a>
						</div>
					</div>
					<div class="card-content">
						<?php if (isset($error)) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-danger fade in">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<?php echo $error; ?>
									</div>
								</div>
							</div>
						<?php } ?>
						<form action="add" method="post" enctype="multipart/form-data">
							<?php if($catID != null) { ?>
								<div class="row">
									<div class="col-md-6">
										<label class="control-label"> Category Name<span style="color : red;">*</span></label>
										<span  class="form-control" ><?php echo ucfirst($category->category_name);?></span> 
										<input type="hidden" name="category" value="<?php echo $category->id;?>"> 
									</div>
								</div>	
								<div class="row">
									<div class="col-md-6">
										<label class="control-label"> Sub Category Name<span style="color : red;">*</span></label>
										<span  class="form-control" ><?php echo ucfirst($category->name);?></span> 
										<input type="hidden" name="sub_category" value="<?php echo $catID;?>"> 
									</div>
								</div>	
							<?php } else { ?>
								
								<div class="row">
									<div class="col-md-6">
										<div class="form-group label-floating">
											<label class="control-label">Category <span style="color : red;">*</span></label>
											<select name="category" class="col-md-12 form-control" id="parent-category" required>
											<?php if(count($categories)) { ?>
												<?php foreach($categories as $key => $val) { ?>
													<option value="<?php echo $val->id;?>"> <?php echo ucfirst($val->category_name);?> </option>
												<?php } ?>
											<?php } ?>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group label-floating">
											<label class="control-label"> Sub Category <span style="color : red;">*</span></label>
											<select name="sub_category" class="col-md-12 form-control" id="child-categories" required>
											<?php if(count($sub_categories)) { ?>
												<?php foreach($sub_categories as $key => $val) { ?>
													<option value="<?php echo $val->id;?>"> <?php echo ucfirst($val->name);?> </option>
												<?php } ?>
											<?php } ?>
											</select>
										</div>
									</div>
								</div>
								
							<?php } ?>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Question <span style="color : red;">*</span></label>
										<input type="text" class="form-control required" name="name" required>
									</div>
								</div>
							</div>
							
							<button type="submit" class="btn btn-primary pull-right">Save</button>
							<div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
          
