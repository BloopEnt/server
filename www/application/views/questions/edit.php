 <div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" data-background-color="purple">
						<h4 class="title">Edit Question</h4>
					</div>
					<div class="card-content">
						<?php if (isset($error)) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-danger fade in">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<?php echo $error; ?>
									</div>
								</div>
							</div>
						<?php } 
							$action = ($subID != null) ? 'edit?id='.$subID : 'edit'; 
						?>
						<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Category <span style="color : red;">*</span></label>
										<select name="category" class="col-md-12 form-control" id="parent-category" required>
										<?php if(count($categories)) { ?>
											<?php foreach($categories as $key => $val) { ?>
													<?php if($val->id == $question->cat_id) { ?>
														<option value="<?php echo $val->id;?>" selected> <?php echo ucfirst($val->category_name);?> </option>
													<?php } else { ?>
														<option value="<?php echo $val->id;?>"> <?php echo ucfirst($val->category_name);?> </option>	
													<?php } ?>
											<?php } ?>
										<?php } ?>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Sub Category <span style="color : red;">*</span></label>
										<select name="sub_category" class="col-md-12 form-control" id="child-categories" required>
										<?php if(count($sub_categories)) { ?>
											<?php foreach($sub_categories as $key => $val) { ?>
													<?php if($val->id == $question->sub_cat_id) { ?>
														<option value="<?php echo $val->id;?>" selected> <?php echo ucfirst($val->name);?> </option>
													<?php } else { ?>
														<option value="<?php echo $val->id;?>"> <?php echo ucfirst($val->name);?> </option>	
													<?php } ?>
											<?php } ?>
										<?php } ?>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Question <span style="color : red;">*</span></label>
										<input type="text" class="form-control required"  name="name"  value="<?php echo $question->name; ?>" required>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Status <span style="color : red;">*</span></label>
										<?php $status = array('Inactive','Active'); ?>
										<select name="status" class="col-md-12 form-control" required>
											<?php foreach($status as $key => $val) { ?>
												<?php if($key == $question->status) { ?>	
													<option value="<?php echo $key;?>" selected> <?php echo $val;?> </option>
												<?php } else { ?>
													<option value="<?php echo $key;?>"> <?php echo $val;?> </option>
												<?php } ?>	
											<?php } ?>
										</select>
									</div>
								</div>
							</div>
							<input type="hidden" class="form-control" name="id"  value="<?php echo $question->id; ?>" readonly>
							<?php $q = ($subID != null) ? '?id='.$subID : ''; ?>
							<a href="/index.php/questions/index<?php echo $q;?>" class="btn btn-default pull-right">cancel</a>
							<button type="submit" class="btn btn-primary pull-right">Save</button>
							<div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
          
