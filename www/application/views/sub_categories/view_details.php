 <div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" data-background-color="purple">
						<h4 class="title">View Sub Category</h4>
					</div>
					<div class="card-content">
						<?php if (isset($error)) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-danger fade in">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<?php echo $error; ?>
									</div>
								</div>
							</div>
						<?php } else { ?>
							<form action="" method="post" enctype="multipart/form-data">
								<div class="row">
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-11">
												<div class="form-group label-floating">
													<label class="control-label">Category</label>
													<span class="form-control"><?php echo ucfirst($sub_category->parent_category);?></span>	
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-11">
												<div class="form-group label-floating">
													<label class="control-label">Sub Category Name</label>
													<span class="form-control"><?php echo ucfirst($sub_category->name); ?> </span>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-11">
												<div class="form-group label-floating">
													<label class="control-label">Background Color</label>
													<span class="form-control"> <span style="background-color: <?php echo $sub_category->bg_color ; ?>; padding:8px 16px;"></span> &nbsp;&nbsp;<?php echo $sub_category->bg_color ; ?></span>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-11">
												<div class="form-group label-floating">
													<label class="control-label">Status</label>
													<span class="form-control"><?php echo ($sub_category->status == 1) ? 'Active' : 'Inactive';?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-11">
												<div class="form-group label-floating">
													<label class="control-label">Paid</label>
													<span class="form-control"><?php echo ($sub_category->paid == 1) ? 'Yes' : 'No';?></span>
												</div>
											</div>
										</div>
										<?php if($sub_category->paid == 0){ 
											$paid = "hide";
										} else {
											$paid= "";
										} ?>
										<div class="row <?php echo $paid;?>">
											<div class="col-md-11">
												<div class="form-group label-floating">
													<label class="control-label">IOS Identifier</label>
													<span class="form-control"><?php echo $sub_category->ios_app_id ; ?></span>
												</div>
											</div>
										</div>
										<div class="row <?php echo $paid;?>">
											<div class="col-md-11">
												<div class="form-group label-floating">
													<label class="control-label">Android Identifier</label>
													<span class="form-control"><?php echo $sub_category->andrdoid_app_id ; ?></span>
												</div>
											</div>
										</div>
										<div class="row <?php echo $paid;?>">
											<div class="col-md-11">
												<div class="form-group label-floating">
													<label class="control-label">Price</label>
													<span class="form-control"><?php echo $sub_category->price  ; ?></span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php if($catID != null) { ?>
									<a href="/index.php/subCategories/index?id=<?php echo $catID; ?>" class="btn btn-default pull-right">Cancel</a>
									<a href="/index.php/subCategories/edit/<?php echo $sub_category->id; ?>?catID=<?php echo $catID; ?>" class="btn btn-warning pull-right">edit</a>
								<? } else { ?>
									<a href="/index.php/subCategories/index" class="btn btn-default pull-right">Cancel</a>
									<a href="/index.php/subCategories/edit/<?php echo $sub_category->id; ?>" class="btn btn-warning pull-right">edit</a>
								<?php } ?>
								<a href="/index.php/questions/add?catID=<?php echo $sub_category->id; ?>" class="btn btn-primary pull-right">Add Questions</a>
								<div class="clearfix"></div>
							</form>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
          
