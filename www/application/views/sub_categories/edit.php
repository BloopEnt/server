 <div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" data-background-color="purple">
						<h4 class="title">Edit Sub Category</h4>
					</div>
					<div class="card-content">
						<?php if (isset($error)) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-danger fade in">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<?php echo $error; ?>
									</div>
								</div>
							</div>
						<?php } 
						
						$action = ($catID != null) ? 'edit?catID='.$catID : 'edit'; 
						?>
						<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-11">
											<div class="form-group">
												<label class="control-label">Category <span style="color : red;">*</span></label>
												<select name="category" class="col-md-12 form-control" required>
												<?php if(count($categories)) { ?>
													<?php foreach($categories as $key => $val) { ?>
														<?php if($sub_category->parent_id == $val->id)  {?>
																<option value="<?php echo $val->id;?>" selected > <?php echo ucfirst($val->category_name);?> </option>
														<?php } else {?>
																<option value="<?php echo $val->id;?>" > <?php echo ucfirst($val->category_name);?> </option>

														<?php } ?>
													<?php } ?>
												<?php } ?>
												</select>
											</div>
										</div>
									</div>	
									<div class="row">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">Sub Category Name <span style="color : red;">*</span></label>
												<input type="text" class="form-control required" name="name" value="<?php echo $sub_category->name; ?>" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-10">
											<div class="form-group ">
												<label class="control-label">Background Color <span style="color : red;">*</span></label>
												<input type="text" class="form-control required color-input" name="bg_color" value="<?php echo $sub_category->bg_color; ?>" required>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group color-picker-container">
												<span class="show-selected-color" style="background-color: <?php echo $sub_category->bg_color ; ?>; padding:6px 12px;"></span>
												<img src="<?php echo base_url(); ?>assets/images/color-palette.png" class="color-picker" title="Choose Color">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">Status</label>
												<?php $status = array('Inactive','Active'); ?>
												<select name="status" class="col-md-12 form-control" required>
													<?php foreach($status as $key => $val) { ?>
														<?php if($key == $sub_category->status) { ?>	
															<option value="<?php echo $key;?>" selected> <?php echo $val;?> </option>
														<?php } else { ?>
															<option value="<?php echo $key;?>"> <?php echo $val;?> </option>
														<?php } ?>	
													<?php } ?>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">Paid</label>
												<?php $paid = array('No','Yes'); ?>
												<select name="paid" id="paid" class="col-md-12 form-control">
													<?php foreach($paid as $key => $val) { ?>
														<?php if($key == $sub_category->paid) { ?>	
															<option value="<?php echo $key;?>" selected> <?php echo $val;?> </option>
														<?php } else { ?>
															<option value="<?php echo $key;?>"> <?php echo $val;?> </option>
														<?php } ?>	
													<?php } ?>
												</select>
											</div>
										</div>
									</div>
									<div class="row paid">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">IOS Identifier <span style="color : red;">*</span></label>
												<input type="text" class="form-control required paid-enable" name="ios_app_id" value="<?php echo $sub_category->ios_app_id; ?>">
												<span> Hint : Please enter your APP purchase ID</span>
											</div>
										</div>
									</div>
									<div class="row paid">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">Android Identifier <span style="color : red;">*</span></label>
												<input type="text" class="form-control required paid-enable" name="andrdoid_app_id" value="<?php echo $sub_category->andrdoid_app_id; ?>" >
												<span> Hint : Please enter your APP purchase ID</span>
											</div>
										</div>
									</div>
									<div class="row paid">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">Price</label>
												<input type="text" class="form-control paid-enable" name="price" value="<?php echo $sub_category->price; ?>" >
											</div>
										</div>
									</div>
								</div>
							
							</div>
							<input type="hidden" class="form-control" name="id" value="<?php echo $sub_category->id; ?>" readonly>
							<?php if($catID != null) { ?>
									<a href="/index.php/subCategories/index?id=<?php echo $catID; ?>" class="btn btn-default pull-right">Cancel</a>
							<?php } else { ?>
									<a href="/index.php/subCategories/index" class="btn btn-default pull-right">Cancel</a>
							<?php } ?>		
							<button type="submit" class="btn btn-primary pull-right save">Save</button>
							<div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
          
