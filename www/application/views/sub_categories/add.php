 <div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" data-background-color="purple">
						<h4 class="title">Add Sub Category</h4>
					</div>
					<div class="card-content">
						<?php if (isset($error)) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-danger fade in">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<?php echo $error; ?>
									</div>
								</div>
							</div>
						<?php } 
						$action = ($catID != null) ? 'add?id='.$catID : 'add'; 
						?>
						<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-6">
									<?php if($catID != null) { ?>
										<div class="row">
											<div class="col-md-11">
												<label class="control-label"> Category <span style="color : red;">*</span></label>
												<span  class="form-control" ><?php echo ucfirst($category->category_name);?></span>
												<input type="hidden" name="category" value="<?php echo $catID;?>"> 
											</div>
										</div>
										
									<?php } else { ?>
										<div class="row">
											<div class="col-md-11">
												<div class="form-group">
													<label class="control-label">Category <span style="color : red;">*</span></label>
													<select name="category" class="col-md-12 form-control" required>
													<?php if(count($categories)) { ?>
														<?php foreach($categories as $key => $val) { ?>
															<option value="<?php echo $val->id;?>"> <?php echo ucfirst($val->category_name);?> </option>
														<?php } ?>
													<?php } ?>
													</select>
												</div>
											</div>
										</div>
									<?php } ?>
									<div class="row">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label"> Sub Category Name <span style="color : red;">*</span></label>
												<input type="text" class="form-control required" name="name" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-10">
											<div class="form-group ">
												<label class="control-label">Background Color <span style="color : red;">*</span></label>
												<input type="text" class="form-control color-input required" name="bg_color" required>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group color-picker-container">
												<span class="show-selected-color"></span>
												<img src="<?php echo base_url(); ?>assets/images/color-palette.png" class="color-picker" title="Choose Color"></span>
											</div>
										</div>
									</div>	
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">Paid</label>
												<select name="paid" id="paid" class="col-md-12 form-control">
													<option value="0"> No </option>
													<option value="1"> Yes </option>
												</select>
											</div>
										</div>
									</div>
									<div class="row paid">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">IOS Identifier <span style="color : red;">*</span></label>
												<input type="text" class="form-control required paid-enable" name="ios_app_id"/>
												<span> Hint : Please enter your APP purchase ID</span>
											</div>
										</div>
									</div>
									<div class="row paid">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">Android Identifier <span style="color : red;">*</span></label>
												<input type="text" class="form-control required paid-enable" name="andrdoid_app_id"/>
												<span> Hint : Please enter your APP purchase ID</span>
											</div>
										</div>
									</div>
									<div class="row paid">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">Price</label>
												<input type="text" class="form-control paid-enable" name="price"/>
											</div>
										</div>
									</div>
									
								</div>	
							
							</div>
							
							<button type="submit" class="btn btn-primary pull-right save">Save</button>
							<div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
          
                  
