<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header row" data-background-color="purple">
						
						<h4 class="title col-sm-6">Sub Categories <?php if($parentCategoryName != null) { echo " - ".ucfirst($parentCategoryName); }?></h4>
						
						<div class="col-sm-6">
						<?php if($parentCatID != null) { ?>
							<a class="pull-right btn btn-primary" href="add?id=<?php echo $parentCatID; ?>" >  Add </a>
						<?php } else { ?>
							<a class="pull-right btn btn-primary" href="add" >  Add </a>
						<?php } ?>
						</div>
					</div>
					<div class="card-content table-responsive">
						<table class="table">
							<thead class="text-primary">
								<th>Name</th>
								<th>Category</th>
								<th>Color</th>
								<th>No of Questions</th>
								<th>Paid</th>
								<th>Price</th>
								<th>Status</th>
								<th>Created Date</th>
								<th>Action</th>
							</thead>
							<tbody>
							<?php if(count($subCategories)) { ?>
								<?php foreach($subCategories as $subCategory) {?>
								<tr>
									<td><?php echo $subCategory->name;?></td>
									<td> <a href="/index.php/categories/view/<?php echo $subCategory->parent_id;?>"><?php echo ucfirst($subCategory->category->category_name);?></a></td>
									<td ><span style="background-color :<?php echo $subCategory->bg_color;?>; padding :8px 16px;"></span></td>
									<td><a href="/index.php/questions/index?id=<?php echo $subCategory->id; ?>"><?php echo $subCategory->questions;?></a></td>
									<td><?php echo ($subCategory->paid == 1) ? 'Yes' : 'No';?></td>
									<td><?php echo $subCategory->price ;?></td>
									<td><?php echo ($subCategory->status == 1) ? 'Active' : 'Inactive';?></td>
									<td><?php echo date('m/d/Y',strtotime($subCategory->created_date));?></td>
									<td class="text-primary"><?php 
										$catId = $subCategory->id;
										if($parentCatID != null) {
											echo anchor('subCategories/view/' . $catId.'?catID='.$parentCatID, '<i class="fa fa-eye"></i>', array('class' => "btn btn-success btn-sm", 'title' => 'View'));
											echo "&nbsp;";
											echo anchor('subCategories/edit/' . $catId.'?catID='.$parentCatID, '<i class="fa fa-pencil"></i>', array('class' => "btn btn-primary btn-sm", 'title' => 'Edit'));
										} else {
											echo anchor('subCategories/view/' . $catId, '<i class="fa fa-eye"></i>', array('class' => "btn btn-success btn-sm", 'title' => 'View'));
											echo "&nbsp;";
											echo anchor('subCategories/edit/' . $catId, '<i class="fa fa-pencil"></i>', array('class' => "btn btn-primary btn-sm", 'title' => 'Edit'));
										}
									?>
									</td>
								</tr>
							<?php
								}
							 } else { ?>
								<tr colspan="7"> No Record found in this table</tr>
							 
						<?php } ?>
							</tbody>
						</table>
						<?php if (isset($links)) { 
								echo $links; 
							  }
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
                       
