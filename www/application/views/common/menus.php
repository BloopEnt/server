<!-- **********************************************************************************************************************************************************
          MAIN SIDEBAR MENU
          *********************************************************************************************************************************************************** -->
<!--sidebar start-->
<div class="sidebar" data-color="purple" data-image="<?php echo base_url(); ?>assets/images/background_image.png">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
	<div class="logo">
		<a href="" class="simple-text">
			<img src="<?php echo base_url(); ?>assets/images/logo.png" style="height:100px;width:200px;"  />
		</a>
	</div>
	<div class="sidebar-wrapper">
		<ul class="nav">
			<li class="<?php if($class == 'dashboard') { echo 'active';} ?>">
				<a href="/index.php/users/index">
					<i class="material-icons"></i>
					<p>Dashboard</p>
				</a>
			</li>
			<li class="<?php if($class == 'categories') { echo 'active';} ?>">
				<a href="/index.php/categories/index">
					<i class="material-icons"></i>
					<p>Categories</p>
				</a>
			</li>
			<li class="<?php if($class == 'SubCategories') { echo 'active';} ?>">
				<a href="/index.php/subCategories/index">
					<i class="material-icons"></i>
					<p>Sub Categories</p>
				</a>
			</li>
			<li class="<?php if($class == 'Questions') { echo 'active';} ?>" >
				<a href="/index.php/questions/index">
					<i class="material-icons"></i>
					<p>Questions</p>
				</a>
			</li>
		</ul>
	</div>
</div>
<!--sidebar end-->
<div class="main-panel">
<nav class="navbar navbar-transparent navbar-absolute">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"> <?php echo $title;?> </a>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li>
					<a data-url="/index.php/users/logout" onclick="javascript : confirmBox(this,'Are you sure? You want to logout from this account')" title="Logout">
						<i class="material-icons">person</i>
					</a>
				</li>
			</ul>
		</div>
	</div>
</nav>
<?php if($this->router->fetch_class() != 'users') {?>
<div class="content">
	<div class="col-sm-12 col-md-12 col-xs-12">
		<?php
		if ($this->router->fetch_method() != 'dashboard') {
			$varController = $this->router->fetch_class();
			if (isset($bread_controller)) {
				$varController = $bread_controller;
			}
			?>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="/index.php/users">Home</a></li>
				<li class="breadcrumb-item active"><a href="/index.php<?php echo $url; ?>"><?= $varController; ?></a></li>
				<?php if ($this->router->fetch_method() != 'index') { ?>
					<li class="breadcrumb-item"><?= ucfirst($this->router->fetch_method()); ?></li>
						<?php
					}
				?>
			</ol>
			<?php
		}
		?>
	</div>
</div>
<?php } ?>

