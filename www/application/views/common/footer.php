
			<footer class="footer">
			</footer>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/material.min.js" type="text/javascript"></script>
<!--  Notifications Plugin    -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap-notify.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="<?php echo base_url(); ?>assets/js/material-dashboard.js?v=1.2.0"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?php echo base_url(); ?>assets/js/demo.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/colorpicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('.color-picker').ColorPicker({
			onSubmit: function(hsb, hex, rgb, el) {
				$('.color-input').val('#'+hex);
				$('.show-selected-color').css('backgroundColor', '#' + hex);
				$('.show-selected-color').css('padding', '6px 12px');
			}
			
		}); 
	
        if($('#search').val() != '' && $('#search').val() != null) {
			$('.cross').removeClass('hide');
		}
        
        if($('#paid').val() == 0) {
				$('.paid').addClass('hide');
				$('.paid-enable').removeAttr('required','required');
			} else {
				$('.paid').removeClass('hide');
				$('.paid-enable').attr('required','required');
			}
		$('#paid').change(function(){
			if($(this).val() == 0) {
				$('.paid').addClass('hide');
				$('.paid-enable').removeAttr('required','required');
			} else {
				$('.paid').removeClass('hide');
				$('.paid-enable').attr('required','required');
			}
		});
		
		$(document).off('click','#add-more-questions').on('click','#add-more-questions',function(){
			$(this).addClass('hide');
			$('.last-added-question').addClass('hide');
			$('.new-question-add').removeClass('hide');
			$('#save-questions').removeClass('hide');
		});
		
		var parentCatID = $('#parent-category').val(); 
		getSubCategories(parentCatID);
	
		
		$('#parent-category').change(function(){
			var parentCatID = $(this).val(); 
			getSubCategories(parentCatID);
		});
		
		$(document).off('click','.save').on('click','.save',function(){
			var color = $('.color-input').val();
			if(color != '') {			
				if (/^(#[a-f0-9]{3}([a-f0-9]{3})?)$/.test(color) == false) {
					$('.color-input').val('');
					alert("Background color value must be a hex code");
				}
			}
			if(trim.$('#question').val() == null) {
				$(this).val('');
				$(this).parent().addClass('is-empty has-error');
				return false;
			}
			
		});
		
		$(document).off('change','.upload-image').on('change','.upload-image', function(){
			var reader = new FileReader();
			reader.onload = imageIsLoaded;
			reader.readAsDataURL(this.files[0]);
		});
		
		$(document).off('change','.required').on('change','.required', function(){
			if($.trim($(this).val()) == ''){
				$(this).val('');
				$(this).parent().addClass('is-empty has-error is-focused');
			}
		});
		
		$('#search').change(function(){
			var search = $(this).val();
			var subId = "<?php echo isset($subID)? $subID : '' ?>";
			if(subId != null && subId != '') {
				var url = "/index.php/questions/index?id="+subId+"&search="+search;
			} else {
				var url = "/index.php/questions/index?search="+search;
			}
			window.location.href = url;
		});		
		
		$('.cross').on('click',function() {
			var subId = "<?php echo isset($subID)? $subID : '' ?>";
			if(subId != null && subId != '') {
				var url = "/index.php/questions/index?id="+subId;
			} else {
				var url = "/index.php/questions/index";
			} 
			window.location.href = url;
		});
			
		
    });
    
    function imageIsLoaded(e){
		$('.add-Cat-image').removeClass('hide');
		$('.img-responsive').attr('src', e.target.result);
		
	}
    
    function getSubCategories(parentCatID){
		$.ajax({
			url:'<?php echo base_url()."index.php/SubCategories/getAjaxSubCategories/"; ?>'+parentCatID,
			dataType: 'html',
			type: 'get',
			success : function(response){
				$('#child-categories').html('');
				$('#child-categories').html(response);					
			},
			error : function(error){
				console.log(error);
			}
		
		});

	}
	
	function confirmBox(obj,msg) {
		var url = $(obj).attr('data-url');
		var result = confirm(msg);
		if(result == true){
			window.location = url;
		}
	}
</script>

</html>
