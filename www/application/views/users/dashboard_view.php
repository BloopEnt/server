<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="card card-stats">
					<div class="card-header" data-background-color="orange">
						<i class="material-icons">content_copy</i>
					</div>
					<div class="card-content">
						<p class="category">Categories</p>
						<h3 class="title"><?php echo $categories;?></h3>
					</div>
					<div class="card-footer">
						<div class="stats">
							<i class="material-icons text-danger"></i>
							<a href="/index.php/categories/index">View All</a>
						</div>
					</div>
				</div>
			</div>      
		
			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="card card-stats">
					<div class="card-header" data-background-color="green">
						<i class="material-icons">assignment</i>
					</div>
					<div class="card-content">
						<p class="category">Sub Categories</p>
						<h3 class="title"><?php echo $sub_cat;?></h3>
					</div>
					<div class="card-footer">
						<div class="stats">
							<i class="material-icons"></i>
							<a href="/index.php/subCategories/index">View All</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="card card-stats">
					<div class="card-header" data-background-color="red">
						<i class="material-icons">help</i>
					</div>
					<div class="card-content">
						<p class="category">Questions</p>
						<h3 class="title"><?php echo $questions;?></h3>
					</div>
					<div class="card-footer">
						<div class="stats">
							<i class="material-icons"></i>
							<a href="/index.php/questions/index">View All</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	
