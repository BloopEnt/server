<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
		<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Straight Up | Admin Panel</title>
		
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
		<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/style_dashboard.css" rel="stylesheet">
		
    </head>
	<body>
        <div class="wrapper wrapper-full-page">
			<div id="full-page login-page">
				<div class="container">
					<div class="container login-content">
						<div class="row">
							<div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
								<div class="card card-signup">
									 <?php echo form_open('users/login', array('class' => 'form-login')); ?>
										<div class="card-header card-header-primary text-center" data-background-color="purple">
											<h4 class="card-title">Straight Up Admin Login</h4>
										</div>
										<div class="card-content">
											<?php if (isset($varError)) { ?>
											<div class="alert alert-danger fade in">
												<a href="#" class="close" data-dismiss="alert">&times;</a>
												<?php echo $varError; ?>.
											</div>
											<?php } ?>
											<div class="input-group">
												<span class="input-group-addon">
													<i class="material-icons">mail</i>
												</span>
												<div class="form-group label-floating">		
													<input type="email" class="form-control" name ='email' placeholder="Email Address" required value="<?php echo set_value('email'); ?>">
													<?php echo form_error('email', "<p style='color:red'>", "</p>"); ?>
												</div>
											</div>
											<div class="input-group">
												<span class="input-group-addon">
													<i class="material-icons">lock_outline</i>
												</span>
												<div class="form-group label-floating">	
													<input type="password" class="form-control" placeholder="Password" required  name='password'>
													<?php echo form_error('password', "<p style='color:red'>", "</p>"); ?> 
													<br/>
												</div>
											</div>
										</div>
										<div class="footer text-center">
											<button type="submit" class="btn btn-primary btn-link btn-wd btn-lg">SIGN IN</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>	
				</div>	
			</div>
		</div>
    </body>
    <!--   Core JS Files   -->
	<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/material.min.js" type="text/javascript"></script>
	<!--  Charts Plugin -->
	<script src="<?php echo base_url(); ?>assets/js/chartist.min.js"></script>
	<!--  Dynamic Elements plugin -->
	<script src="<?php echo base_url(); ?>assets/js/arrive.min.js"></script>
	<!--  PerfectScrollbar Library -->
	<script src="<?php echo base_url(); ?>assets/js/perfect-scrollbar.jquery.min.js"></script>
	<!--  Notifications Plugin    -->
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-notify.js"></script>
	<!-- Material Dashboard javascript methods -->
	<script src="<?php echo base_url(); ?>assets/js/material-dashboard.js?v=1.2.0"></script>
	<!-- Material Dashboard DEMO methods, don't include it in your project! -->
	<script src="<?php echo base_url(); ?>assets/js/demo.js"></script>
</html>
