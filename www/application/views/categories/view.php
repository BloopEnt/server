<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header row" data-background-color="purple">
						
						<h4 class="title col-sm-6">Categories</h4>
						
						<div class="col-sm-6">
							<a class="pull-right btn btn-primary" href="add" >  Add </a>
						</div>
					</div>
					<div class="card-content table-responsive">
						<table class="table">
							<thead class="text-primary">
								<th>Name</th>
								<th>Image</th>
								<th>Color</th>
								<th>No of Sub Categories</th>
								<th>Order</th>
								<th>Status</th>
								<th>Created Date</th>
								<th>Action</th>
							</thead>
							<tbody>
							<?php if(count($categories)) { ?>
								<?php foreach($categories as $category) {?>
								<tr>
									<td><?php echo $category->category_name;?></td>
									<td><img class="cat-img" style="height:50px; width:50px; border-radius:25px;" src = "<?php echo base_url(); ?>assets/uploads/<?php echo $category->image; ?>" alt = 'Category Image' /></td>
									<td ><span style="background-color :<?php echo $category->bg_color;?>; padding :8px 16px;"></span></td>
									<td><a href="/index.php/subCategories/index?id=<?php echo $category->id;?>"><?php echo $category->subCategories;?></a></td>
									<td><?php echo $category->ordering;?></td>
									<td><?php echo ($category->status == 1) ? 'Active' : 'Inactive';?></td>
									<td><?php echo date('m/d/Y',strtotime($category->created_date));?></td>
									<td class="text-primary"><?php 
										$catId = $category->id;
                                        echo anchor('categories/view/' . $catId, '<i class="fa fa-eye"></i>', array('class' => "btn btn-success btn-sm", 'title' => 'View'));
                                        echo "&nbsp;";
                                        echo anchor('categories/edit/' . $catId, '<i class="fa fa-pencil"></i>', array('class' => "btn btn-primary btn-sm", 'title' => 'Edit'));
									?>
<!--
										<button class="btn btn-danger btn-sm delete" onclick="javascript: confirmBox(this,'Are you sure, You want to delete this category')" data-url="/index.php/categories/delete/<?php echo $catId;?>" title = "Delete" ><i class="fa fa-trash-o"></i></button>
-->
									</td>
								</tr>
							<?php
								}
							 } else { ?>
								<tr colspan="10"> No Record found in this table</tr>
							 
						<?php } ?>
							</tbody>
						</table>
						 <?php if (isset($links)) { 
									echo $links; 
								} 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
                       
