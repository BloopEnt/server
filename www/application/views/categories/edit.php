 <div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" data-background-color="purple">
						<h4 class="title">Edit Category</h4>
					</div>
					<div class="card-content">
						<?php if (isset($error)) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-danger fade in">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<?php echo $error; ?>
									</div>
								</div>
							</div>
						<?php } ?>
						<form action="add" method="post" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-11">
											<div class="form-group">
												<label class="control-label"> Category Name <span style="color : red;">*</span></label>
												<input type="text" class="form-control required" name="name" value="<?php echo $category->category_name; ?>" required>
											</div>
										</div>
									</div>	
								
									<div class="row">
										<div class="col-md-10">
											<div class="form-group ">
												<label class="control-label">Background Color <span style="color : red;">*</span></label>
												<input type="text" class="form-control required color-input" name="bg-color" value="<?php echo $category->bg_color; ?>" required>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group color-picker-container" >
												<span class="show-selected-color"style="background-color: <?php echo $category->bg_color ; ?>; padding:6px 12px;"></span>
												<img src="<?php echo base_url(); ?>assets/images/color-palette.png" class="color-picker" title="Choose Color">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-11">
											<div class="form-group label-floating">
												<label class="control-label">Instructions <span style="color : red;">*</span></label>
												<textarea class="form-control required" rows="3" name="instructions" required ><?php echo ucfirst($category->instructions);?></textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">Status</label>
												<?php $status = array('Inactive','Active'); ?>
												<select name="status" class="col-md-12 form-control" required>
													<?php foreach($status as $key => $val) { ?>
														<?php if($key == $category->status) { ?>	
															<option value="<?php echo $key;?>" selected> <?php echo $val;?> </option>
														<?php } else { ?>
															<option value="<?php echo $key;?>"> <?php echo $val;?> </option>
														<?php } ?>	
													<?php } ?>
												</select>									
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-11">
											<div class="form-group">
												<label class="control-label">Order <span style="color : red;">*</span></label>
												<input type="number" class="form-control" name="ordering" min="1" value="<?php echo $category->ordering; ?>" required>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-11">
											<div class="form-group cat-image ">
												<label class="control-label">Image</label>
												<input type="file" class="form-control upload-image" name="image">
											</div>
										</div>
<!--
										<div class="col-md-2">
											<div class="form-group cat-image ">
												<img src="<?php echo base_url(); ?>assets/uploads/<?php echo $category->image; ?>" class="img-responsive cat-thumbnail"  />
											</div>
										</div>
-->
									</div>	
									<div class="row">
										<div class="col-md-11">
											<div class="form-group cat-image ">
												<img src="<?php echo base_url(); ?>assets/uploads/<?php echo $category->image; ?>" class="img-responsive"  />
											</div>
										</div>
									</div>
								
<!--
									<div class="row">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">Paid</label>
												<?php $paid = array('No','Yes'); ?>
												<select name="paid" id="paid" class="col-md-12 form-control">
													<?php foreach($paid as $key => $val) { ?>
														<?php if($key == $category->paid) { ?>	
															<option value="<?php echo $key;?>" selected> <?php echo $val;?> </option>
														<?php } else { ?>
															<option value="<?php echo $key;?>"> <?php echo $val;?> </option>
														<?php } ?>	
													<?php } ?>
												</select>
											</div>
										</div>
									</div>
									<div class="row paid">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">IOS Identifier <span style="color : red;">*</span></label>
												<input type="text" class="form-control required paid-enable" name="ios_app_id" value="<?php echo $category->ios_app_id; ?>">
												<span> Hint : Please enter your APP purchase ID</span>
											</div>
										</div>
									</div>
									<div class="row paid">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">Android Identifier <span style="color : red;">*</span></label>
												<input type="text" class="form-control required paid-enable" name="andrdoid_app_id" value="<?php echo $category->andrdoid_app_id; ?>" >
												<span> Hint : Please enter your APP purchase ID</span>
											</div>
										</div>
									</div>
									<div class="row paid">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">Price</label>
												<input type="text" class="form-control paid-enable" name="price" value="<?php echo $category->price; ?>" >
											</div>
										</div>
									</div>
-->
								</div>
							</div>
							<input type="hidden" name="id" value="<?php echo $category->id; ?>" />
							<input type="hidden" name="previous_image" value="<?php echo $category->image; ?>" />
							<a href="/index.php/categories/index" class="btn btn-default pull-right">Cancel</a>

							<button type="submit" class="btn btn-primary pull-right save">Save</button>
							<div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
          
