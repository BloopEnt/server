 <div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" data-background-color="purple">
						<h4 class="title">View Category</h4>
					</div>
					
					<div class="card-content">
						<?php if (isset($error)) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-danger fade in">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<?php echo $error; ?>
									</div>
								</div>
							</div>
						<?php } else { ?>
							<form action="" method="post" enctype="multipart/form-data">
								<div class="row">
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-11">
												<div class="form-group label-floating">
													<label class="control-label"> Category Name</label>
													<span class="form-control"><?php echo ucfirst($category->category_name); ?></span>
												</div>
											</div>
										</div>	
										<div class="row">
											<div class="col-md-11">
												<div class="form-group label-floating">
													<label class="control-label">Background Color</label>
													<span class="form-control"> <span style="background-color: <?php echo $category->bg_color ; ?>; padding:8px 16px;"></span> &nbsp;&nbsp;<?php echo $category->bg_color ; ?></span>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-11">
												<div class="form-group label-floating">
													<label class="control-label">Instructions</label>
													<textarea class="form-control" readonly rows="3"><?php echo ucfirst($category->instructions);?> </textarea>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-11">
												<div class="form-group label-floating">
													<label class="control-label">Status</label>
													<span class="form-control"><?php echo ($category->status == 1) ? 'Active' : 'Inactive';?></span>
												</div>
											</div>
										</div>
										
									</div>
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-11">
												<div class="form-group label-floating">
													<label class="control-label"> Category Order</label>
													<span class="form-control"><?php echo $category->ordering; ?></span>
												</div>
											</div>
										</div>
										
										<?php if ($category->image != null &&  $category->image != '') { ?>
										<div class="row">
											<div class="col-md-11">
												<div class="form-group cat-image label-floating">
													<label class="control-label">Image</label>
													<img  src="<?php echo base_url(); ?>assets/uploads/<?php echo $category->image; ?>"  class="img-responsive" />
												</div>
											</div>
										</div>
										<?php } ?>		
<!--
										<div class="row">
											<div class="col-md-11">
												<div class="form-group label-floating">
													<label class="control-label">Paid</label>
													<span class="form-control"><?php echo ($category->paid == 1) ? 'Yes' : 'No';?></span>
												</div>
											</div>
										</div>
										<?php if($category->paid == 0){ 
											$paid = "hide";
										} else {
											$paid= "";
										} ?>
										<div class="row <?php echo $paid;?>">
											<div class="col-md-11">
												<div class="form-group label-floating">
													<label class="control-label">IOS Identifier</label>
													<span class="form-control"><?php echo $category->ios_app_id ; ?></span>
												</div>
											</div>
										</div>
										<div class="row <?php echo $paid;?>">
											<div class="col-md-11">
												<div class="form-group label-floating">
													<label class="control-label">Android Identifier</label>
													<span class="form-control"><?php echo $category->andrdoid_app_id ; ?></span>
												</div>
											</div>
										</div>
										<div class="row <?php echo $paid;?>">
											<div class="col-md-11">
												<div class="form-group label-floating">
													<label class="control-label">Price</label>
													<span class="form-control"><?php echo $category->price  ; ?></span>
												</div>
											</div>
										</div>
-->
									</div>
								</div>
								<a href="/index.php/categories/index" class="btn btn-default pull-right">Cancel</a>
								<a href="/index.php/subCategories/add?catID=<?php echo $category->id; ?>" class="btn btn-primary pull-right">Add Sub Categories</a>
								<a href="/index.php/categories/edit/<?php echo $category->id; ?>" class="btn btn-warning pull-right">Edit</a>
								<div class="clearfix"></div>
							</form>
						<?php } ?>
					</div>
		
				</div>
			</div>
		</div>
	</div>
</div>
          
