 <div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" data-background-color="purple">
						<h4 class="title">Add Category</h4>
					</div>
					<div class="card-content">
						<?php if (isset($error)) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="alert alert-danger fade in">
										<a href="#" class="close" data-dismiss="alert">&times;</a>
										<?php echo $error; ?>
									</div>
								</div>
							</div>
						<?php } ?>
						<form action="add" method="post" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-11">
											<div class="form-group">
												<label class="control-label"> Category Name <span style="color : red;">*</span></label>
												<input type="text" class="form-control required" name="name" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-10">
											<div class="form-group ">
												<label class="control-label">Background Color <span style="color : red;">*</span></label>
												<input type="text" class="form-control required color-input" name="bg-color" required>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group color-picker-container" >
												<span class="show-selected-color"></span>
												<img src="<?php echo base_url(); ?>assets/images/color-palette.png" class="color-picker" title="Choose Color">
												
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-11">
											<div class="form-group">
												<label class="control-label">Instructions <span style="color : red;">*</span> </label>
												<textarea class="form-control required" name="instructions" required></textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-11">
											<div class="form-group">
												<label class="control-label">Image <span style="color : red;">*</span> </label>
												<input type="file" class="form-control upload-image" name="image" required>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-11">
											<div class="form-group">
												<label class="control-label">Order <span style="color : red;">*</span></label>
												<input type="number" class="form-control" name="ordering" min="1" required>
											</div>
										</div>
									</div>
									<div class="row add-Cat-image hide">
										<div class="col-md-11">
											<div class="form-group">
												<img src="" class="img-responsive"  />
											</div>
										</div>
									</div>
									
<!--
									<div class="row">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">Paid</label>
												<select name="paid" id="paid" class="col-md-12 form-control">
													<option value="0"> No </option>
													<option value="1"> Yes </option>
												</select>
											</div>
										</div>
									</div>
									<div class="row paid">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">IOS Identifier <span style="color : red;">*</span></label>
												<input type="text" class="form-control required paid-enable" name="ios_app_id"/>
												<span> Hint : Please enter your APP purchase ID</span>
											</div>
										</div>
									</div>
									<div class="row paid">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">Android Identifier <span style="color : red;">*</span></label>
												<input type="text" class="form-control required paid-enable" name="andrdoid_app_id"/>
												<span> Hint : Please enter your APP purchase ID</span>
											</div>
										</div>
									</div>
									<div class="row paid">
										<div class="col-md-11">
											<div class="form-group ">
												<label class="control-label">Price</label>
												<input type="text" class="form-control paid-enable" name="price"/>
											</div>
										</div>
									</div>
-->
									
								</div>	
							</div>
							
							<button type="submit" class="btn btn-primary pull-right save">Save</button>
							<div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
          
