<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class StraightUp extends CI_Controller {
	 
	function __construct()
    {
		parent::__construct();
    } 
    
	public function index($type = null)
	{
		$this->load->model("FuelPriceAdsModel");
		$arrData	=	array();
		
		
		$arrData['results'] 	=	$result;
	
		$this->sendResponse($arrData);
	
	}
	
	public function getCategories(){
		$arrData	=	array();
		$this->load->model('Categories_model');
        $categories		= $this->Categories_model->list_all_api();
        $this->load->model('SubCategories_model');
        $sub_categories = $this->SubCategories_model->list_all_api();
        $this->load->model('Questions_model');
        $questions 		= $this->Questions_model->list_all_api();
		$arrData['categories'] 	=	$categories;
		$arrData['sub_categories'] 	=	$sub_categories;
		$arrData['questions'] 	=	$questions;
	
		$this->sendResponse($arrData);
	}


    function sendResponse($arrData, $status = 200) {
        $this->output
                ->set_status_header($status)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($arrData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }
    
    
    
    public function getCategoriesBackup(){
		$arrData	=	array();
		$this->load->model('Categories_model');
        $categories		= $this->Categories_model->list_all();
        if(count($categories)) { 
			foreach($categories as $key => $category)  {
				$this->load->model('SubCategories_model');
				$sub_categories = $this->SubCategories_model->list_all($category->id);
				$categories[$key]->sub_categories = $sub_categories;
				if(count($sub_categories)) {
					foreach($sub_categories as $k => $sub_category)  {
						$this->load->model('Questions_model');
						$questions = $this->Questions_model->list_all($sub_category->id);
						$categories[$key]->sub_categories[$k]->questions = $questions;
					}
				}
			}
		}
		//echo "<pre>"; print_r($categories); die("dfhdgf");
		$arrData['results'] 	=	$categories;
	
		$this->sendResponse($arrData);
	}

}
