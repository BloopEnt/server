<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/PHPExcel.php');




class Questions extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->helper('url');
        $user = $this->session->userdata('login_user');
		if(is_null($user)){ 
			if($this->router->fetch_method() != 'login'){ 
				redirect('users/login');
			}
		} 
    }

    public function index() {
		$catID =  null;
		$search = '';
		$parentCategoryName = null;
		$this->load->model('Questions_model');
		$url = '/questions/index';
		$data['subID'] = null;
		if(isset($_GET['id']) ) {
			$catID = $_GET['id'];
			$this->load->model('SubCategories_model');
			$parentCategoryName = $this->SubCategories_model->viewSubCategoryDetails($catID);
			$parentCategoryName = $parentCategoryName->name;
			$url = '/questions/index?id='.$_GET['id'];
			$data['subID'] = $catID;
		} 
		if(isset($_GET['search']) ) { 
			$search = $_GET['search'];
		} 
		
		$data['title'] 		= "Questions";
        $data['website'] 	= "Straight Up"; 
        $data['class'] 		= "Questions"; 
		$data['bread_controller'] = "Questions"; 
		$limit_per_page = 100;
		$questions = array();
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3)) : 0;
        $total_records 		= $this->Questions_model->get_total($catID,$search);
		if ($total_records > 0) 
        {
            // get current page records
            $questions = $this->Questions_model->list_all($catID,$limit_per_page, $page,$search);
           
			$config['base_url'] = base_url() . 'index.php/questions/index/';
		
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config['uri_segment'] = 3;
            $config['reuse_query_string'] = true;
            $this->pagination->initialize($config);
             
            // build paging links
            $data['links'] = $this->pagination->create_links();
        }
        if(count($questions)) {
			foreach ($questions  as $key => $question) {
				$this->load->model('SubCategories_model');
				$sub_category = $this->SubCategories_model->viewSubCategoryDetails($question->sub_cat_id);
				$questions[$key]->sub_category = $sub_category;
				$this->load->model('Categories_model');
				$questions[$key]->category = $this->Categories_model->viewCategoryDetails($sub_category->parent_id);
			}
		}
		$data['url'] = $url;
		$data['questions'] = $questions;
		$data['search'] = $search;
		$data['parentCategoryName'] = $parentCategoryName;
        $this->load->view('common/header',$data);
        $this->load->view('common/menus',$data);
        $this->load->view('questions/view');    
        $this->load->view('common/footer');
    }

    public function add() {
		$data = array();
		$this->load->helper('form');
        $this->load->library('form_validation');
        if ($this->input->post()) {
            try {
                $this->form_validation->set_rules('name', 'Question', 'required');
                $this->form_validation->set_rules('sub_category', 'Sub Category', 'required');

                if ($this->form_validation->run()) {
					
					$arrData = [
						'name' 	=> $this->input->post('name'),
						'sub_cat_id' => $this->input->post('sub_category'),
					];
					
                    $this->load->model('Questions_model');
                    $lastInsertID	= $this->Questions_model->add($arrData); 
                    if ($lastInsertID) {
                        $this->session->set_flashdata('success', 'Question added successfully');
                        redirect('/questions/addMoreQuestions/'.$lastInsertID);
                    }
                } else {
					$data['error'] = validation_errors();
				}
            } catch (Exception $ex) {
                $data['error'] = $ex->getMessage();
            }
        }
		$data['title'] 		= "Questions";
        $data['website'] 	= "Straight Up"; 
        $data['class'] 		= "Questions"; 
        $data['bread_controller'] = "Questions"; 

        $this->load->model('SubCategories_model');
        $data['sub_categories'] = $this->SubCategories_model->allSubCategories();
        $this->load->model('Categories_model');
        $data['categories'] = $this->Categories_model->allCategories();
        $data['url'] = '/questions/index';
        $data['catID'] = Null;
        if(isset($_GET['catID'])) {
			$catID = $_GET['catID'];
			$data['category'] = $this->SubCategories_model->getSubCategoryName($catID);
			$data['catID'] = $catID;
			$data['url'] = '/questions/index?id='.$_GET['catID'];
		} 
		if(isset($_GET['id'])) {
			$data['url'] = '/questions/index?id='.$_GET['id'];
		} 
        $this->load->view('common/header',$data);
        $this->load->view('common/menus',$data);
        $this->load->view('questions/add');
        $this->load->view('common/footer');
    }
	
	public function view($id) {
		$data['title'] 		= "Questions";
        $data['website'] 	= "Straight Up"; 
        $data['class'] 		= "Questions"; 
        $data['bread_controller'] = "Questions"; 

        if($id) {
			$this->load->model('Questions_model');
			$question	= $this->Questions_model->viewQuestionWithCategories($id);
			if($question) {
				$data['question'] 		= $question;
				$this->load->model('Categories_model');
				$category	= $this->Categories_model->viewCategoryDetails($question->cat_id);
				if($category){
					$data['question']->cat_name = $category->category_name;
				}	
			} else {
				redirect('/questions/index');
			}
		} else {
			redirect('/questions/index');
		}
		if(isset($_GET['id'])) {
			$data['url'] = '/questions/index?id='.$_GET['id'];
		} else {
			$data['url'] = '/questions/index';
		}
        $this->load->view('common/header',$data);
        $this->load->view('common/menus',$data);
        $this->load->view('questions/view_details');    
        $this->load->view('common/footer');
    }
    
    public function edit($id) {
		$data = array();
		$this->load->helper('form');
        $this->load->library('form_validation');
        $data['url'] = '/questions/index';
		$data['subID'] = null;
		if(isset($_GET['id'])) {
			$data['subID'] = $_GET['id'];
			$data['url'] = '/questions/index?id='.$_GET['id'];
		} 
        if ($this->input->post()) {
            try {
                $this->form_validation->set_rules('name', 'Category Name', 'required');
                $this->form_validation->set_rules('sub_category', 'Category', 'required');
                if ($this->form_validation->run()) {
                    $arrData = [
                        'name' 	=> $this->input->post('name'),
                        'sub_cat_id' => $this->input->post('sub_category'),
                        'status' => $this->input->post('status')
                    ];
                    $this->load->model('Questions_model');
                    if ($this->Questions_model->edit($this->input->post('id'),$arrData)) {
                        $this->session->set_flashdata('success', 'Question saved successfully');
                        redirect($data['url']);
                    }
                } else {
					$data['error'] = validation_errors();
				}
            } catch (Exception $ex) {
                $data['error'] = $ex->getMessage();
            }
        }
		$data['title'] 		= "Questions";
        $data['website'] 	= "Straight Up"; 
        $data['class'] 		= "Questions"; 
        $data['bread_controller'] = "Questions"; 

        $this->load->model('SubCategories_model');
        $data['sub_categories'] = $this->SubCategories_model->allSubCategories();
        $this->load->model('Categories_model');
        $data['categories'] = $this->Categories_model->allCategories();
		if($id) {
			$this->load->model('Questions_model');
			$question	= $this->Questions_model->viewQuestionWithCategories($id);
			if($question) {
				 $data['question'] 	= $question;
			} else {
				redirect($data['url']);
			}
		} else {
			redirect($data['url']);
		}
        $this->load->view('common/header',$data);
        $this->load->view('common/menus',$data);
        $this->load->view('questions/edit');
        $this->load->view('common/footer');
    }
    
    public function addMoreQuestions($lastInsertiId) {
		if($lastInsertiId) {
			$this->load->model('Questions_model');
			$question	= $this->Questions_model->viewQuestionWithCategories($lastInsertiId);
			if($question) {
				$data['question'] 		= $question;
				$this->load->model('Categories_model');
				$category	= $this->Categories_model->viewCategoryDetails($question->cat_id);
				if($category){
					$data['question']->cat_name = $category->category_name;
				}	
				 
			} else {
				$data['error'] = "Invalid Question";
			}
		} else {
			$data['error'] = "Invalid Question";
		}
		$data['title'] 		= "Questions";
        $data['website'] 	= "Straight Up"; 
        $data['class'] 		= "Questions"; 
		$data['bread_controller'] = "Questions"; 
		$data['url'] = '/questions/index';
        $this->load->view('common/header',$data);
        $this->load->view('common/menus',$data);
        $this->load->view('questions/add_more_questions');
        $this->load->view('common/footer');
	
	}
	
	public function import() {
		$data = array();
		$this->load->helper('form');
        $this->load->library('form_validation');
        if ($this->input->post()) {
            try {
                $this->form_validation->set_rules('sub_category', 'Sub Category', 'required');

               if ($this->form_validation->run()) { 
					$this->load->library('upload');
                    $varImage = $varLogo = "";
					if (array_key_exists('excel-sheet', $_FILES) && $_FILES['excel-sheet']['name'] != '') {
                     
						$this->excel = new PHPExcel(); 
						$file_info = pathinfo($_FILES["excel-sheet"]["name"]);
						//$file_directory = "./assets/excel_sheets";
						$file_directory = "./assets/excel_sheets/questions_";
						$new_file_name = date("d-m-Y ") . rand(000000, 999999) .".". $file_info["extension"];

						if(move_uploaded_file($_FILES["excel-sheet"]["tmp_name"], $file_directory . $new_file_name))
						{   
							$file_type	= PHPExcel_IOFactory::identify($file_directory . $new_file_name);
							$objReader	= PHPExcel_IOFactory::createReader($file_type);
							$objPHPExcel = $objReader->load($file_directory . $new_file_name);
							$sheet_data	= $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
							$arrData = [];
							if(count($sheet_data) > 0) {
								foreach($sheet_data as $row) {
									$record = array();
									$record['name'] 		= $row['A'];
									$record['sub_cat_id'] 	= $this->input->post('sub_category');
									$arrData[] = $record; 
								}
							}
							$this->load->model('Questions_model');
							if ($this->Questions_model->addMultipleQuestions($arrData)) {
								$this->session->set_flashdata('success', 'Questions saved successfully');
								redirect('/questions/index?id='.$this->input->post('sub_category'));
							}	
						}
							
                    } else {
						$data['error'] = "Please select atleast one excel sheet";
					}
                } else {
					$data['error'] = validation_errors();
				}
            } catch (Exception $ex) {
                $data['error'] = $ex->getMessage();
            }
        }
		$data['title'] 		= "Questions";
        $data['website'] 	= "Straight Up"; 
        $data['class'] 		= "Questions"; 
        $data['bread_controller'] = "Questions"; 

        //~ $this->load->model('SubCategories_model');
        //~ $data['sub_categories'] = $this->SubCategories_model->allSubCategories();
        $this->load->model('Categories_model');
        $data['categories'] = $this->Categories_model->allActiveCategories();
        if(isset($_GET['id'])) {
			$data['url'] = '/questions/index?id='.$_GET['id'];
		} else {
			$data['url'] = '/questions/index';
		}
        $this->load->view('common/header',$data);
        $this->load->view('common/menus',$data);
        $this->load->view('questions/import');
        $this->load->view('common/footer');
    }
  

}
