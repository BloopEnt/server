<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct() {
       parent::__construct();
       $user = $this->session->userdata('login_user');
	   if(is_null($user)){ 
			if($this->router->fetch_method() != 'login'){ 
				redirect('users/login');
			}
	   } 
    }

    public function index() {
		$data['title'] 		= "Dashbaord";
        $data['website'] 	= "Straight Up"; 
        $data['class'] 		= "dashboard"; 
        $this->load->model('Categories_model');
        $categories			=	$this->Categories_model->allCategories();
        $this->load->model('SubCategories_model');
        $sub_categories		=	$this->SubCategories_model->allSubCategories();
        $this->load->model('Questions_model');
        $questions			=	$this->Questions_model->allQuestions();
        $data['categories'] = count($categories);
        $data['sub_cat'] 	= count($sub_categories);
        $data['questions'] 	= count($questions);
        $this->load->view('common/header',$data);
        $this->load->view('common/menus',$data);
        $this->load->view('users/dashboard_view');
        $this->load->view('common/footer');
    }

    public function login() {
        $data = array();
        $this->load->helper('form');
        $this->load->library('form_validation');
        if ($this->input->post()) {
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run()) {
                $this->load->model('Users_model');
                $arrData = $this->Users_model->checkUserInfo();
                if ($arrData) {
                    $newdata = array(
                        'username' => $arrData[0]['name'],
                        'email' => $arrData[0]['email'],
                        'logged_in' => TRUE
                    );
                    $this->session->set_userdata('login_user',$newdata);
                    redirect('users/index');
                } else {
                    $data['varError'] = "Email/Password is wrong";
                }
            }
        }
        $this->load->view('users/login_view', $data);
    }

    public function logout() {
        $this->session->set_userdata('login_user','');
        $this->session->sess_destroy('login_user');
        redirect('users/login');
    }

}
