<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SubCategories extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->helper('url');
        $user = $this->session->userdata('login_user');
		if(is_null($user)){ 
			if($this->router->fetch_method() != 'login'){ 
				redirect('users/login');
			}
		} 
    }

    public function index() {
		$catID = $data['parentCatID'] =  null;
		$parentCategoryName = null;
		$url = '/subCategories/index';
		if(isset($_GET['id'])) {
			$catID = $_GET['id'];
			$data['parentCatID'] = $catID;
			$this->load->model('Categories_model');
			$parentCategoryName = $this->Categories_model->getCategoryName($catID);
			$parentCategoryName = $parentCategoryName->category_name;
			$url = '/subCategories/index?id='.$_GET['id'];
		} 
		$data['title'] 		= "Sub Categories";
        $data['website'] 	= "Straight Up"; 
        $data['class'] 		= "SubCategories"; 
		$data['bread_controller'] = "Sub Categories"; 
		$subCategories = array();
        $this->load->model('SubCategories_model');
        $limit_per_page = 10;
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3)) : 0;
        $total_records 		= $this->SubCategories_model->get_total($catID);
		if ($total_records > 0) 
        {
            // get current page records
            $subCategories		= $this->SubCategories_model->list_all($catID,$limit_per_page, $page);
             
            $config['base_url'] 	= base_url() . 'index.php/subCategories/index';
            $config['total_rows'] 	= $total_records;
            $config['per_page'] 	= $limit_per_page;
            $config['uri_segment'] 	= 3;
            $config['reuse_query_string'] = true;
            $this->pagination->initialize($config);
             
            // build paging links
            $data['links'] = $this->pagination->create_links();
        }
        
        if(count($subCategories)) {
			foreach ($subCategories  as $key => $subCategory) {
				$this->load->model('Categories_model');
				$subCategories[$key]->category = $this->Categories_model->getCategoryName($subCategory->parent_id);
				$this->load->model('Questions_model');
				$subCategories[$key]->questions = $this->Questions_model->QuestionsCount($subCategory->id);
			}
		}
		$data['url'] = $url;
		$data['subCategories']	= $subCategories;
		$data['parentCategoryName']	= $parentCategoryName;
        $this->load->view('common/header',$data);
        $this->load->view('common/menus',$data);
        $this->load->view('sub_categories/view');    
        $this->load->view('common/footer');
    }

    public function add() {
		$data = array();
		$url = null;
		$this->load->helper('form');
        $this->load->library('form_validation');
        if ($this->input->post()) {
            try {
                $this->form_validation->set_rules('name', 'Sub Category Name', 'required');
                $this->form_validation->set_rules('bg_color', 'Background Color', 'required');
                $this->form_validation->set_rules('category', 'Category', 'required');
                if($this->input->post('paid') == 1) {
					$this->form_validation->set_rules('price', 'Price', 'required');
					$this->form_validation->set_rules('andrdoid_app_id', 'Android Identifier', 'required');
					$this->form_validation->set_rules('ios_app_id', 'IOS Identifier', 'required');
				}
                if ($this->form_validation->run()) {
                    $arrData = [
                        'name' 	=> $this->input->post('name'),
                        'bg_color' => $this->input->post('bg_color'),
                        'parent_id' => $this->input->post('category'),
                        'price' => $this->input->post('price'),
                        'paid' => $this->input->post('paid'),
                        'ios_app_id' => $this->input->post('ios_app_id'),
                        'andrdoid_app_id' => $this->input->post('andrdoid_app_id'),
                    ];
                    $this->load->model('SubCategories_model');
                    $lastInsertID	= $this->SubCategories_model->add($arrData);
                    if ($lastInsertID) {
                        $this->session->set_flashdata('success', 'Sub Category added successfully');
                        $url = '/subCategories/view/'.$lastInsertID;
                        redirect($url);
                    }
                } else {
					$data['error'] = validation_errors();
				}
            } catch (Exception $ex) {
                $data['error'] = $ex->getMessage();
            }
        }
		$data['title'] 		= "Sub Categories";
        $data['website'] 	= "Straight Up"; 
        $data['class'] 		= "SubCategories";
		$data['bread_controller'] = "Sub Categories"; 
        $this->load->model('Categories_model');
        $data['categories']	= $this->Categories_model->allCategories();	
       if(isset($_GET['catID'])) {
			$catID = $_GET['catID'];
			$data['category'] = $this->Categories_model->getCategoryName($catID);
			$data['catID'] = $catID;
		} else {
			$data['catID'] = Null;
		}
		if(isset($_GET['id'])) {
			$url = '/subCategories/index?id='.$_GET['id'];
		} else {
			$url = '/subCategories/index';
		}
		$data['url'] = $url;
        $this->load->view('common/header',$data);
        $this->load->view('common/menus',$data);
        $this->load->view('sub_categories/add');
        $this->load->view('common/footer');
    }
	
	public function view($id) {
		$data['title'] 		= "Sub Categories";
        $data['website'] 	= "Straight Up"; 
        $data['class'] 		= "SubCategories"; 
		$data['bread_controller'] = "Sub Categories"; 
		$data['catID'] = null;
		$url = '/subCategories/index';
		if(isset($_GET['catID'])) {
			$url = '/subCategories/index?id='.$_GET['catID'];
			$data['catID'] = $_GET['catID'];
		}
        if($id) {
			$this->load->model('SubCategories_model');
			$sub_category	= $this->SubCategories_model->viewSubCategoryDetails($id);
			if($sub_category) {
				 $this->load->model('Categories_model');
				 $category_name = $this->Categories_model->getCategoryName($sub_category->parent_id);
				 $sub_category->parent_category	= $category_name->category_name;	
				$data['sub_category'] = $sub_category;
			} else {
				redirect($url);
			}
		} else {
			redirect($url);
		}
		$data['url'] = $url;
        $this->load->view('common/header',$data);
        $this->load->view('common/menus',$data);
        $this->load->view('sub_categories/view_details');    
        $this->load->view('common/footer');
    }
    
    public function edit($id) {
		$data = array();
		$this->load->helper('form');
        $this->load->library('form_validation');
        $url = '/subCategories/index';
        $data['catID'] = null; 
        if(isset($_GET['catID'])) {
			$url = '/subCategories/index?id='.$_GET['catID'];
			$data['catID'] = $_GET['catID'];
		} 
        if ($this->input->post()) {
            try {
                $this->form_validation->set_rules('name', 'Category Name', 'required');
                $this->form_validation->set_rules('bg_color', 'Color', 'required');
                $this->form_validation->set_rules('category', 'Category', 'required');
                if($this->input->post('paid') == 1) {
					$this->form_validation->set_rules('price', 'Price', 'required');
					$this->form_validation->set_rules('andrdoid_app_id', 'Android Identifier', 'required');
					$this->form_validation->set_rules('ios_app_id', 'IOS Identifier', 'required');
				}
                if ($this->form_validation->run()) {
                    $arrData = [
                        'name' 	=> $this->input->post('name'),
                        'bg_color' => $this->input->post('bg_color'),
                        'parent_id' => $this->input->post('category'),
                        'price' => $this->input->post('price'),
                        'paid' => $this->input->post('paid'),
                        'status' => $this->input->post('status'),
                        'ios_app_id' => $this->input->post('ios_app_id'),
                        'andrdoid_app_id' => $this->input->post('andrdoid_app_id'),
                    ];
                    $this->load->model('SubCategories_model');
                    if ($this->SubCategories_model->edit($this->input->post('id'),$arrData)) {
                        $this->session->set_flashdata('success', 'Sub Category saved successfully');
                        redirect($url);
                    }
                } else {
					$data['error'] = validation_errors();
				}
            } catch (Exception $ex) {
                $data['error'] = $ex->getMessage();
            }
        }
        $data['url'] = $url;
		$data['title'] 		= "Sub Categories";
        $data['website'] 	= "Straight Up"; 
        $data['class'] 		= "SubCategories";
        $data['bread_controller'] = "Sub Categories"; 
 
        $this->load->model('Categories_model');
        $data['categories']	= $this->Categories_model->allCategories();	
         if($id) {
			$this->load->model('SubCategories_model');
			$sub_category	= $this->SubCategories_model->viewSubCategoryDetails($id);
			if($sub_category) {
				$data['sub_category'] = $sub_category;
			} else {
				redirect($url);
			}
		} else {
			redirect($url);
		}
        $this->load->view('common/header',$data);
        $this->load->view('common/menus',$data);
        $this->load->view('sub_categories/edit');
        $this->load->view('common/footer');
    }
    
    public function getAjaxSubCategories($catID) {
		$dropDown = '';
		if($catID) {
			$this->load->model('SubCategories_model');
			$subCategories		= $this->SubCategories_model->list_all_active_subCategories($catID);
			if(count($subCategories)) {
				foreach($subCategories as $key => $val) {
					$dropDown .= '<option value="'.$val->id.'">'.ucfirst($val->name).'</option>' ;
				}
				
				$response	=	json_encode(array('subCategories'=>$subCategories,'status' => 'success','msg'=>''));
			} else {
				$dropDown .= '<option value="">Select Sub Category</option>' ;
			}
		} else {
			$dropDown .= '<option value="">Select Sub Category</option>' ;
		}
		echo $dropDown;
		exit;
	}
  

}
