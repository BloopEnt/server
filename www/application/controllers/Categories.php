<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->helper('url');
        $user = $this->session->userdata('login_user');
	    if(is_null($user)){ 
			if($this->router->fetch_method() != 'login'){ 
				redirect('users/login');
			}
	    } 
    }

    public function index() {
		$data['title'] 		= "Categories";
        $data['website'] 	= "Straight Up"; 
        $data['class'] 		= "categories"; 
        $data['bread_controller'] = "Categories"; 
        $this->load->model('Categories_model');
        
        $limit_per_page = 10;
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3)) : 0;
        $total_records 		= $this->Categories_model->get_total();
		if ($total_records > 0) 
        {
            // get current page records
            $categories		= $this->Categories_model->list_all($limit_per_page, $page);
             
            $config['base_url'] 	= base_url() . 'index.php/categories/index';
            $config['total_rows'] 	= $total_records;
            $config['per_page'] 	= $limit_per_page;
            $config['uri_segment'] 	= 3;
            $this->pagination->initialize($config);
             
            // build paging links
            $data['links'] = $this->pagination->create_links();
        }
        if(count($categories)) {
			foreach ($categories  as $key => $category) {
				$this->load->model('SubCategories_model');
				$categories[$key]->subCategories = $this->SubCategories_model->subCategoryCount($category->id);
			}
		}
		$data['url'] = '/categories/index';
        $data['categories']	= $categories;
        $this->load->view('common/header',$data);
        $this->load->view('common/menus',$data);
        $this->load->view('categories/view');    
        $this->load->view('common/footer');
    }

    public function add() {
		$data = array();
		$this->load->helper('form');
        $this->load->library('form_validation');
        if ($this->input->post()) {
            try {
                $this->form_validation->set_rules('name', 'Category Name', 'required');
                $this->form_validation->set_rules('bg-color', 'Color', 'required');
                $this->form_validation->set_rules('instructions', 'Instructions', 'required');
                $this->form_validation->set_rules('ordering', 'Order', 'required');
                if($this->input->post('paid') == 1) {
					$this->form_validation->set_rules('price', 'Price', 'required');
					$this->form_validation->set_rules('andrdoid_app_id', 'Android Identifier', 'required');
					$this->form_validation->set_rules('ios_app_id', 'IOS Identifier', 'required');
				}
                if ($this->form_validation->run()) {
                    $this->load->library('upload');
                    $varImage = $varLogo = "";
                   
                    if (array_key_exists('image', $_FILES) && $_FILES['image']['name'] != '') {
                        $varFileName 				= time() . rand()."_".$_FILES['image']['name'];
                        $config['upload_path'] 		= './assets/uploads';
                        $config['allowed_types'] 	= 'gif|jpg|png|jpeg';
                        $config['file_name'] 		= $varFileName;
                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload('image')) {
                            throw new Exception($this->upload->display_errors());
                        }
                        $varImage = $this->upload->data('orig_name');
                    }
                    
                    $arrData = [
                        'category_name' 	=> $this->input->post('name'),
                        'bg_color' => $this->input->post('bg-color'),
                        'instructions' => $this->input->post('instructions'),
                        'image' => $varImage,
                        'ordering' => $this->input->post('ordering'),
                        'price' => $this->input->post('price'),
                        'paid' => $this->input->post('paid'),
                        'ios_app_id' => $this->input->post('ios_app_id'),
                        'andrdoid_app_id' => $this->input->post('andrdoid_app_id'),
                    ];
                    $this->load->model('Categories_model');
                    $lastInsertID = $this->Categories_model->add($arrData);
                    if ($lastInsertID) {
						if($this->updateOrder($lastInsertID,$arrData['ordering'])) {
						
							$this->session->set_flashdata('success', 'Category added successfully');
							redirect('/categories/view/'.$lastInsertID);
						} else {
							$this->session->set_flashdata('error', 'Something went wrong on category order');
							redirect('/categories/view/'.$lastInsertID);
						}
                    }
                } else {
					$data['error'] = "Please Fill all required Fields";
				}
            } catch (Exception $ex) {
                $data['error'] = $ex->getMessage();
            }
        }
        $data['url'] = '/categories/index';
		$data['title'] 		= "Categories";
		$data['website'] 	= "Straight Up"; 
        $data['class'] 		= "categories"; 
		$data['bread_controller'] = "Categories"; 
        $this->load->view('common/header',$data);
        $this->load->view('common/menus',$data);
        $this->load->view('categories/add');
        $this->load->view('common/footer');
    }	
    
     public function view($id) {
		$data['title'] 		= "Categories";
        $data['website'] 	= "Straight Up"; 
        $data['class'] 		= "categories"; 
        $data['bread_controller'] = "Categories"; 

        if($id) {
			$this->load->model('Categories_model');
			$category	= $this->Categories_model->viewCategoryDetails($id);
			if($category) {
				$data['category'] = $category;
				$data['url'] = '/categories/index';
				$this->load->view('common/header',$data);
				$this->load->view('common/menus',$data);
				$this->load->view('categories/view_details');    
				$this->load->view('common/footer');
			} else {
				redirect('/categories/index');
			}
		} else {
			redirect('/categories/index');
		}
    }
    
     public function edit($id) {
		
		$data = array();
		$this->load->helper('form');
        $this->load->library('form_validation');
        if ($this->input->post()) {
            try {
                $this->form_validation->set_rules('name', 'Category Name', 'required');
                $this->form_validation->set_rules('bg-color', 'Color', 'required');
                $this->form_validation->set_rules('instructions', 'Instructions', 'required');
                $this->form_validation->set_rules('ordering', 'Order', 'required');
                if($this->input->post('paid') == 1) {
					$this->form_validation->set_rules('price', 'Price', 'required');
					$this->form_validation->set_rules('andrdoid_app_id', 'Android Identifier', 'required');
					$this->form_validation->set_rules('ios_app_id', 'IOS Identifier', 'required');
				}
                if ($this->form_validation->run()) {
                    $this->load->library('upload');
                    $varImage = $varLogo = "";
                   
                    if (array_key_exists('image', $_FILES) && $_FILES['image']['name'] != '') {
                        $varFileName 				= time() . rand()."_".$_FILES['image']['name'];
                        $config['upload_path'] 		= './assets/uploads';
                        $config['allowed_types'] 	= 'gif|jpg|png|jpeg';
                        $config['file_name'] 		= $varFileName;
                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload('image')) {
                            throw new Exception($this->upload->display_errors());
                        }
                        $varImage = $this->upload->data('orig_name');
                    }
                    if($varImage) {
						$arrData = [
							'category_name' => $this->input->post('name'),
							'bg_color' 		=> $this->input->post('bg-color'),
							'instructions' 	=> $this->input->post('instructions'),
							'status' 		=> $this->input->post('status'),
							'ordering' 		=> $this->input->post('ordering'),
							'price' 		=> $this->input->post('price'),
							'paid' 			=> $this->input->post('paid'),
							'ios_app_id'	=> $this->input->post('ios_app_id'),
							'andrdoid_app_id' => $this->input->post('andrdoid_app_id'),
							'image' 		=> $varImage
						];
					} else {
						$arrData = [
							'category_name' => $this->input->post('name'),
							'bg_color' 		=> $this->input->post('bg-color'),
							'instructions'	=> $this->input->post('instructions'),
							'status' 		=> $this->input->post('status'),
							'ordering' 		=> $this->input->post('ordering'),
							'price' 		=> $this->input->post('price'),
							'paid' 			=> $this->input->post('paid'),
							'ios_app_id' 	=> $this->input->post('ios_app_id'),
							'andrdoid_app_id' => $this->input->post('andrdoid_app_id'),
						];
					}
                    $this->load->model('Categories_model');
                   
                    $existingOrder = $this->Categories_model->viewCategoryDetails($this->input->post('id'));
              
                    if ($this->Categories_model->edit($this->input->post('id'),$arrData)) {
						
						$response = $this->updateOrder($this->input->post('id'),$arrData['ordering'],$existingOrder->ordering);
						
						if($response) {
							if($varImage) {
								@unlink('./assets/uploads/'.$this->input->post('previous_image'));	
							}
							$this->session->set_flashdata('success', 'Category saved successfully');
							redirect('/categories/index');
						} else {
							$this->session->set_flashdata('error', 'Something went wrong on category order');
							redirect('/categories/index');
						}			
                    }
                } else {
					$data['error'] = validation_errors();
				}
            } catch (Exception $ex) {
                $data['error'] = $ex->getMessage();
            }
        }
        
		$data['title'] 		= "Categories";
		$data['website'] 	= "Straight Up"; 
        $data['class'] 		= "categories"; 
        $data['bread_controller'] = "Categories"; 
		$data['url'] 	= '/categories/index';
        if($id) {
			$this->load->model('Categories_model');
			$category	= $this->Categories_model->viewCategoryDetails($id);
			if($category) {
				$data['category'] = $category;
			} else {
				redirect('/categories/index');
			}
		} else {
			redirect('/categories/index');
		}
        $this->load->view('common/header',$data);
        $this->load->view('common/menus',$data);
        $this->load->view('categories/edit');
        $this->load->view('common/footer');
    }	
    
    public function updateOrder($id,$order,$existingOrder = null) {
		$this->load->model('Categories_model');
		if($id != null && $order != null) {
			if($existingOrder != null) {
				if($order < $existingOrder) {
					$records 	= $this->Categories_model->OrderDecrement($id,$order,$existingOrder);
					if(count($records)) {
						foreach($records as $record) {
							$record->ordering = $record->ordering + 1;
							$this->Categories_model->saveUpdatedOrder($record->id,$record); 
						}
					}
				} else if($order > $existingOrder && $order != $existingOrder) {
					$records 	= $this->Categories_model->OrderIncrement($id,$order,$existingOrder);
					if(count($records)) {
						foreach($records as $record) {
							$record->ordering = $record->ordering - 1;
							$this->Categories_model->saveUpdatedOrder($record->id,$record); 
						}
					}
				}
			} else {
				$records 	= $this->Categories_model->updateOrderOnAddCategory($id,$order);
				if(count($records)) {
					foreach($records as $record) {
						$record->ordering = $record->ordering + 1;
						$this->Categories_model->saveUpdatedOrder($record->id,$record); 
					}
				}
			}
			return true;
		} 
	}
   

}
