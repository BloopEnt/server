<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Users_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function checkUserInfo() {
        $email = $this->input->post('email');
        $varPassword = $this->input->post('password');
        $this->db->select('name,email');
        $varUserCheck = $this->db->get_where('users', array('email' => $email, 'password' => md5($varPassword), 'status' => '1'));
        if ($varUserCheck->num_rows() === 1) {
            return $varUserCheck->result_array();
        } else {
            return false;
        }
    }

}
