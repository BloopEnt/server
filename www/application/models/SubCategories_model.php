<?php

defined('BASEPATH') or exit('No direct script access allowed');

class SubCategories_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
	public function list_all($catID = null,$limit,$start) 
    {
        $this->db->order_by('id', 'desc');
        $this->db->limit($limit, $start);
        if($catID != null) {
			
			$query = $this->db->get_where('sub_categories', array('parent_id' => $catID));
			//die($this->db->last_query());
		} else {
			$query = $this->db->get('sub_categories');
		}
        return $query->result();
    }
    
    public function add($arrData) {
        $this->db->trans_start();
        $this->db->insert('sub_categories', $arrData);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            throw new Exception('Error occured while saving data');
        }
        return $insert_id;
    }
    
    public function subCategoryCount($id){
		$query = $this->db->get_where('sub_categories', array('parent_id' => $id));
        return count($query->result_array());
		
	}
	
	public function allSubCategories(){
		
		$this->db->order_by('id', 'desc');
		$this->db->select('id,name,parent_id');
        $query = $this->db->get_where('sub_categories');
        return $query->result();
	
	}
	public function viewSubCategoryDetails($id){
        $query = $this->db->get_where('sub_categories',array('id'=> $id));
        return $query->row();
	}
	
	public function getCategoryName($id) {
		$this->db->select('name');
		$query = $this->db->get_where('sub_categories',array('id'=> $id));
        return $query->row();
	}
	
	public function edit($id,$arrData) {
	
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('sub_categories', $arrData);
		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            throw new Exception('Error occured while saving data');
        }
        return true;
	}
	
	public function getSubCategoryName ($catID) {
		
		$this->db->select('*,sub_categories.id As sub_cat_id');
		$this->db->join('categories', 'categories.id = sub_categories.parent_id','left');
		$query = $this->db->get_where('sub_categories',array('sub_categories.id'=> $catID));
		return $query->row();	
	}
	
	public function childCategories($catID = null) 
    {
        $this->db->order_by('id', 'desc');
        if($catID != null) {
			
			$query = $this->db->get_where('sub_categories', array('parent_id' => $catID));
			return $query->result_array();
		} 
        
    }
    public function get_total($catID) 
    {	
		if($catID != null){		
			$this->db->where('parent_id',$catID);
		}
		$this->db->from("sub_categories");
		return $this->db->count_all_results();
    }
    
    public function list_all_api() 
    {
        $this->db->order_by('id', 'desc');
		$query = $this->db->get('sub_categories');
        return $query->result();
    }
    
    public function list_all_active_subCategories ($catID) 
    {
        $this->db->order_by('id', 'desc');
		$query = $this->db->get_where('sub_categories', array('parent_id' => $catID,'status' => 1));
        return $query->result();
    }

}
