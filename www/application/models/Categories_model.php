<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Categories_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
	public function list_all($limit,$start) 
    {	
		$this->db->limit($limit, $start);
        $this->db->order_by('ordering', 'asc');
        $query = $this->db->get('categories');
        return $query->result();
    }
    
    public function add($arrData) {
        $this->db->trans_start();
        $this->db->insert('categories', $arrData);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            throw new Exception('Error occured while saving data');
        }
        return $insert_id;
    }
   
	public function allCategories(){
		
		$this->db->order_by('id', 'desc');
		$this->db->select('id, category_name');
        $query = $this->db->get_where('categories');
        return $query->result();
	
	}
	
	public function viewCategoryDetails($id){
        $query = $this->db->get_where('categories',array('id'=> $id));
        return $query->row();
	}
	
	public function getCategoryName($id) {
		$this->db->select('category_name');
		$query = $this->db->get_where('categories',array('id'=> $id));
        return $query->row();
	}
	
	public function edit($id,$arrData) {
	
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('categories', $arrData);
		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            throw new Exception('Error occured while saving data');
        }
        return true;
	}
	
	 public function get_total() 
    {	
		$this->db->from("categories");
		return $this->db->count_all_results();
    }
	
	public function list_all_api() 
    {
        $this->db->order_by('id', 'desc');
		$query = $this->db->get('categories');
        return $query->result();
    }
	
	public function allActiveCategories(){
		
		$this->db->order_by('id', 'desc');
		$this->db->select('id, category_name');
        $query = $this->db->get_where('categories',array('status'=>1));
        return $query->result();
	
	}
	
	public function OrderDecrement($id,$order,$existingOrder) {
        $query = $this->db->get_where('categories',array('ordering >=' => $order,'ordering <' => $existingOrder,'id !=' => $id));
        return $query->result();
	}
	
	public function OrderIncrement($id,$order,$existingOrder) {
        $query = $this->db->get_where('categories',array('ordering <=' => $order,'ordering >' => $existingOrder,'id !=' => $id));
        return $query->result();
	}
	
	public function updateOrderOnAddCategory($id,$order) {
        $query = $this->db->get_where('categories',array('ordering >=' => $order,'id !=' => $id));
        return $query->result();
	}
	
	public function saveUpdatedOrder($id,$arrData) {
	
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('categories', $arrData);
		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            throw new Exception('Error occured while saving data');
        }
        return true;
	}
	
	

}
