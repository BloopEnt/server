<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Questions_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
	public function list_all($catID = null,$limit,$start,$search = null) 
    {
        $this->db->order_by('id', 'desc');
        $this->db->limit($limit, $start);
        if($search != null && $search != '') {
			$this->db->where('name like', '%'.$search.'%');
		}
        if($catID != null) {
			$this->db->where('sub_cat_id',$catID);
			//$query = $this->db->get_where('questions',array('sub_cat_id'=>$catID));
		} 
		$query = $this->db->get('questions');
		
        return $query->result();
    }
    
    public function add($arrData) {
        $this->db->trans_start();
        $this->db->insert('questions', $arrData);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            throw new Exception('Error occured while saving data');
        }
        return $insert_id;
    }
    
    public function QuestionsCount($id){
		$query = $this->db->get_where('questions', array('sub_cat_id' => $id));
        return count($query->result_array());
		
	}
	
	public function allQuestions(){
		
		$this->db->select('id');
        $query = $this->db->get_where('questions');
        return $query->result();
	
	}
	
	public function edit($id,$arrData) {
	
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('questions', $arrData);
		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            throw new Exception('Error occured while saving data');
        }
        return true;
	}
	
	public function viewQuestionWithCategories($id){
		
		$this->db->select('s.id As sub_cat_id,s.parent_id as cat_id,q.name as q_name,s.name as s_name,q.*');
		$this->db->join('sub_categories As s', 's.id = q.sub_cat_id','left');
		$query = $this->db->get_where('questions as q',array('q.id'=> $id));
		return $query->row();
	}
	
	public function get_total($catID,$search = null) 
    {	
		if($catID != null){		
			$this->db->where('sub_cat_id',$catID);
		}
		if($search != null && $search != '') {
			$this->db->where('name like','%'.$search.'%');
		}
		$this->db->from("questions");
		return $this->db->count_all_results();
    }
    
    public function list_all_api() 
    {
        $this->db->order_by('id', 'desc');
		$query = $this->db->get('questions');
        return $query->result();
    }
    
     public function addMultipleQuestions($arrData) {
        $this->db->trans_start();
        $this->db->insert_batch('questions', $arrData);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            throw new Exception('Error occured while saving data');
        }
        return true;
    }
    
    

}
